import EnhancedTable from './EnhancedTable';
import React from 'react';
import renderer from 'react-test-renderer';
import { shallow , mount } from 'enzyme';
import { ExpansionPanelActions } from '@material-ui/core';

const chatLog = [{ 
  messageId: '22a887be-78dc-45bf-8997-b712d3de4510',
  userId: 'e837c9f5-247f-445f-bcc3-7d434348336b',
  fullName: 'Martin Bradley',
  timestamp: '2017-01-26T07:53:12Z',
  email: 'mbradley0@google.it',
  message: 'Cras in purus eu magna vulputate luctus.',
  avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff' 
}]

const header = [
  { id: 'messageId', numeric: false, disablePadding: false, label: 'Message Id' },
  { id: 'userId', numeric: false, disablePadding: false, label: 'User Id' },
  { id: 'fullName', numeric: false, disablePadding: false, label: 'Full Name' },
  { id: 'timestamp', numeric: false, disablePadding: false, label: 'Timestamp' },
  { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
  { id: 'message', numeric: false, disablePadding: false, label: 'Message' },
  { id: 'avatar', numeric: false, disablePadding: false, label: 'Avatar' },
];

it('renders the EnhancedTable component', () => {  
  const tree = renderer.create(
  <EnhancedTable data={chatLog}
    title="Chatlog"
    allowSelection={true}
    header={header}
    hideIdColumn={false}
    idColumn="messageId"
    order="desc"
    orderBy="title"
    rowsPerPage={10}
  />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

it('checks the onRefresh callback', () => {  
  const onDelete = jest.fn(),
    props = {
      data: chatLog,
      title: "Chatlog",
      allowSelection: true,
      header: header,
      hideIdColumn: false,
      idColumn: "messageId",
      order: "desc",
      orderBy: "title",
      rowsPerPage: 10,
      onDelete: onDelete
    },
  wrapper = mount(
    <EnhancedTable {...props}/> 
  )

  wrapper.find('input[type="checkbox"]').last().simulate('click');
  expect(wrapper.find('svg#delete').simulate('click'));
  expect(onDelete.mock.calls.length).toBe(1);
});

it('checks the onRefresh callback', () => {  
  const onReload = jest.fn(),
    props = {
      data: chatLog,
      title: "Chatlog",
      allowSelection: true,
      header: header,
      hideIdColumn: false,
      idColumn: "messageId",
      order: "desc",
      orderBy: "title",
      rowsPerPage: 10,
      onReload: onReload,
    },
  wrapper = mount(
    <EnhancedTable {...props}/> 
  )

  expect(wrapper.find('svg#refresh').simulate('click'));
  expect(onReload.mock.calls.length).toBe(1);
});

