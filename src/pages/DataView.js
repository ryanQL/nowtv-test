import React, { Component } from 'react';
import EnhancedTable from '../components/EnhancedTable';
import getChatLog from '../service';

const header = [
  { id: 'messageId', numeric: false, disablePadding: false, label: 'Message Id' },
  { id: 'userId', numeric: false, disablePadding: false, label: 'User Id' },
  { id: 'fullName', numeric: false, disablePadding: false, label: 'Full Name' },
  { id: 'timestamp', numeric: false, disablePadding: false, label: 'Timestamp' },
  { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
  { id: 'message', numeric: false, disablePadding: false, label: 'Message' },
  { id: 'avatar', numeric: false, disablePadding: false, label: 'Avatar' },
];

class DataView extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: []
    }
  }

  async _loadData() {
    const data = await getChatLog();
    this.setState({ data: data });
  }
  
  componentDidMount() {
    this._loadData();
  }

  handleDelete = (messages) => {
    let chatlog = this.state.data.filter(item => {
      const messageId = item[header[0].id];
      return !messages.includes(messageId);
    })
    this.setState({data: chatlog});
  }

  handleReload = () => {
    this._loadData();
  }

  render() {
    return (
      // https://material-ui.com/demos/tables/
      <EnhancedTable 
        data={this.state.data} 
        title="Chatlog"
        allowSelection={true}
        header={header}
        hideIdColumn={false}
        idColumn="messageId"
        order="desc"
        orderBy="title"
        rowsPerPage={10}
        onDelete={this.handleDelete}
        onReload={this.handleReload}

      />
    )
  }
}

export default DataView;
