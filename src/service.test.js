const chatLog = { 
  messageId: '22a887be-78dc-45bf-8997-b712d3de4510',
  userId: 'e837c9f5-247f-445f-bcc3-7d434348336b',
  fullName: 'Martin Bradley',
  timestamp: '2017-01-26T07:53:12Z',
  email: 'mbradley0@google.it',
  message: 'Cras in purus eu magna vulputate luctus.',
  avatar: 'http://dummyimage.com/100x100.png/5fa2dd/ffffff' 
}

const getChatLog = jest.fn(() => chatLog);

it ('calls the getChatLog once', () => {
  getChatLog()
  expect(getChatLog.mock.calls.length).toBe(1);
});

it('returns the correct format', async () => {
  const message = getChatLog.mock.results[0].value
  expect(typeof message.messageId).toBe('string');
  expect(typeof message.userId).toBe('string');
  expect(typeof message.fullName).toBe('string');
  expect(typeof message.timestamp).toBe('string');
  expect(typeof message.email).toBe('string');
  expect(typeof message.message).toBe('string');
  expect(message.avatar === null || typeof message.avatar === 'string').toBeTruthy();
});