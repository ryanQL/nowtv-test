import { getMessages, getMembers } from './data';

function merge(members, messages){
  let merged = [];
  return new Promise(resolve => {
    members.forEach(member => {
      messages.forEach(message => {
        if (member.id === message.userId) {
          merged.push({
            messageId: message.id,
            userId: member.id,
            fullName: [member.firstName, member.lastName].join(' '),
            timestamp: message.timestamp,
            email: member.email,
            message: message.message,
            avatar: member.avatar,
          });
        };
      });
    });

    merged.sort(function(a,b){
      // Turn your strings into dates, and then subtract them
      // to get a value that is either negative, positive, or zero.
      return new Date(b.date) - new Date(a.date);
    });
    
    resolve(merged);
  });
}

export default async function getChatLog() {
  const messages = await getMessages();
  const members = await getMembers();
  const merged = await merge(members, messages);
  return merged;
}
