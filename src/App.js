import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import DataView from './pages/DataView';
import NTVAppBar from './components/NTVAppBar';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header>
            <NTVAppBar />
          </header>
          <section>
            <div>
              <Route exact path="/" component={DataView} />
            </div>
          </section>
        </div>
      </Router>
    );
  }
}

export default App;
